# AMH Language

[![pipeline status](https://gitlab.com/angular-material-home/angular-material-home-language/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/angular-material-home-language/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/72eb6664764f44a19a91c246e8c45785)](https://www.codacy.com/app/angular-material-home/angular-material-home-language?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/angular-material-home-language&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/72eb6664764f44a19a91c246e8c45785)](https://www.codacy.com/app/angular-material-home/angular-material-home-language?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/angular-material-home-language&utm_campaign=Badge_Coverage)

Adds features to manage languages.

## Install

### Manually

Download content of 'dist' directory of project and place files in your project.

### Bower

Use following command:

	bower install --save angular-material-home-language

### Yeoman

## Development

We are using following tools to develop this module. Before starting development you should install below tools.

- nodejs
- npm
- grunt-cli
- bower

### Preparing to develop

Get source of project:

	git clone https://gitlab.com/angular-material-home/angular-material-home-language.git
	
Now run following commands respectively in root directory of project.

	npm install
	bower install

### Demo 

Run following command to run demo:

	grunt demo

	