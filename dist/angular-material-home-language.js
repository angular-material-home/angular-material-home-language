/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeLanguage', [
	'ngMaterialHome',
]);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeLanguage')

.config(function($translateProvider) {
	/*
	 * Setting miss item 
	 */
	$translateProvider.useMissingTranslationHandler('AmhLanguageHandlerFactory');
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeLanguage')
/**
 * State of the user management
 */
.config(function($routeProvider) {
    $routeProvider
    /**
     * @ngdoc ngRoute
     * @name /home/:language
     * @description Main page of the site
     */
    .when('/home/:language', {
        templateUrl : 'views/amh-content.html',
        controller : 'AmhContentCtrl'
    })
    /**
     * @ngdoc ngRoute
     * @name /home/:language
     * @description Main page of the site
     */
    .when('/home', {
        templateUrl : 'views/amh-content.html',
        controller : 'AmhContentCtrl'
    })

    /**
     * @ngdoc ngRoute
     * @name /preferences/languages/manager
     * @description Load language manager
     * 
     * Manages languages and allow user to add a new one.
     */
    .when('/preferences/languages/manager', {
        templateUrl : 'views/amh-setting/languages-manager.html',
        controller : 'AmhLanguagesCtrl',
        /*
         * Check if user is owner
         * @ngInject
         */
        protect: function($rootScope){
            return !$rootScope.app.user.owner;
        }
    })
    ;//
});

  /*
   * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
   * 
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   * 
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   * 
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  'use strict';
  angular.module('ngMaterialHomeLanguage')

          /**
           * @ngdoc controller
           * @name AmhLanguagesCtrl
           * @description Mange list of languages
           * 
           * Manages list of languages
           * 
           */
          .controller('AmhLanguagesCtrl', function ($scope, $rootScope, $language, $navigator, FileSaver) {

              function _load() {
                  return $language.languages()
                          .then(function (langs) {
                              ctrl.items = langs.items;
                              return $language.use();
                          })
                          .then(function (keyLang) {
                              $scope.languageKey = keyLang;
                          });
              }

              /**
               * Set current language of app
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - Key of the language
               * @return {promise} to change language
               */
              function setLanguage(lang) {
//		$rootScope.app.setting.language = lang.key;
                  $language.use(lang.key)//$language is defined in mblowfish-core module
                          .then(function () {
                              $navigator.openView('home/' + lang.key);
                          });
              }

              /**
               * Adds new language to app configuration
               * 
               * @memberof AmhLanguagesCtrl
               * @return {promise} to add language
               */
              function addLanguage() {
                  return $language.newLanguage({
                      key: 'Language key',
                      title: 'Language title'
                  });
              }

              /**
               * Remove language form application
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - The Language
               * @return {promise} to delete language
               */
              function deleteLanguage(lang) {
                  return $language.deleteLanguage(lang)
                          .then(function () {
                              if (angular.equals($scope.selectedLanguage, lang)) {
                                  $scope.selectedLanguage = null;
                              }
                          });
              }

              /**
               * Select language
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - The Language
               */
              function selectLanguage(lang) {
                  $scope.selectedLanguage = lang;
                  lang.map = lang.map || {};
                  angular.forEach($rootScope.app.setting.languageMissIds, function (id) {
                      lang.map[id] = lang.map[id] || id;
                  });
              }

              /**
               * Save a language as a file
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - The Language
               */
              function saveAs(lang) {
                  var MIME_WB = 'application/weburger+json;charset=utf-8';

                  // save  result
                  var dataString = JSON.stringify(lang);
                  var data = new Blob([dataString], {
                      type: MIME_WB
                  });
                  return FileSaver.saveAs(data, 'language.json');
              }

            /**
            * Upload a file as the content of the page
            * 
            * @memberof AmhContentCtrl
            * @return {promiss} to save content value
            */
            function uploadLang(file){
                    var fileInput = document.createElement("input");
                    fileInput.type='file';
                    fileInput.style.display='none';
                    fileInput.onchange=function(event){
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                    var lang = JSON.parse(event.target.result);
                                    $language.newLanguage(lang)
                                        .then(function(lang){
                                                $scope.$apply();
                                         }, function(error){
                                             alert(error);
                                         });
                            };
                            reader.readAsText(event.target.files[0]); 
                    };
                    document.body.appendChild(fileInput);
                    // click Elem (fileInput)
                    var eventMouse = document.createEvent("MouseEvents");
                    eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    fileInput.dispatchEvent(eventMouse);
            }

              
              function close(){
                  $scope.uploadFileIsTrue = false;
              }

              if (!$scope.app) {
                  $scope.app = $rootScope.app;
              }

              // TODO: hadi, This controller should call $language.refresh() after add/remove/update a language

              var ctrl = {};
              $scope.ctrl = ctrl;
              $scope.setLanguage = setLanguage;
              $scope.addLanguage = addLanguage;
              $scope.deleteLanguage = deleteLanguage;
              $scope.selectLanguage = selectLanguage;
              $scope.uploadLang = uploadLang;
              $scope.saveAs = saveAs;
              $scope.close = close;
              _load();
          });
///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//angular.module('ngMaterialHomeLanguage')
//
//.factory('AmhLanguageConfigLoader', function ($q, $rootScope) {
//	return function (option) {
//        var deferred = $q.defer();
//		$rootScope.$on('$appStartSuccess', function(){
//			var app = $rootScope.app;
//			if(!app.config.languages){
//				return deferred.reject('No language registerd');
//			}
//			var lang = null;
//			angular.forEach(app.config.languages, function(item){
//				if(item.key === option.key){
//					lang = item;
//				}
//			});
//			if(!lang){
//				return deferred.reject('Language not found');
//			}
//			var translate = {};
//			angular.forEach(lang.map, function(item){
//				translate[item.key] = item.value;
//			});
//			deferred.resolve(translate);
//		});
//		return deferred.promise;
//	};
//});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeLanguage')

.factory('AmhLanguageHandlerFactory', function ($language, $rootScope) {
	// has to return a function which gets a tranlation ID
	return function (translationID) {
		var app = $rootScope.app;
		var key =$language.use()
		if(!app.setting.languageMissIds){
			app.setting.languageMissIds=[];
		}
//		if(!app.setting.languageMiss[key]){
//			app.setting.languageMiss[key] = {};
//		}
		var index = app.setting.languageMissIds.indexOf(translationID);
	    if (index === -1) {
	    	app.setting.languageMissIds.push(translationID);
	    }
	};
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeLanguage')
/**
 * Adds basic system settings
 * 
 */
.run(function($rootScope, $language) {
	
	/*
	 * Lesson on page
	 */
	$rootScope.$watch(function(){
		return $rootScope.app.setting.language ||
			$rootScope.app.config.local.language ||
			'fa';
	}, function(key){
		return $language.use(key);
	});
	
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeLanguage')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($widget) {

	
	$widget.newWidget({
		type: 'CommonFeatureToolbarLanguage',
		templateUrl : 'views/amh-widgets/feature-toolbar-language.html',
		label : 'Toolbar with language',
		description : 'A toolbar to show actions with sidenav and language.',
		icon : 'wb-common-toolbar',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/toolbar',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['description','common-features'],
	});
});

angular.module('ngMaterialHomeLanguage').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amh-setting/languages-manager.html',
    "<md-sidenav class=md-sidenav-left md-component-id=lanaguage-manager-left md-is-locked-open=true md-whiteframe=4> <md-content> <div style=\"height: 100px\" flex layout=column ng-if=uploadFileIsTrue> <lf-ng-md-file-input lf-files=files> </lf-ng-md-file-input> <div> <md-button class=\"md-raised md-primary\" ng-click=uploadLang(files)> Load </md-button> <md-button ng-click=close()> close </md-button> </div> </div> <md-toolbar> <div class=md-toolbar-tools> <label flex translate>Languages</label> <md-button ng-click=uploadLang() class=md-icon-button aria-label=\"Upload a language\"> <wb-icon>upload</wb-icon> </md-button> <md-button ng-click=addLanguage() class=md-icon-button aria-label=\"Add new language\"> <wb-icon>add</wb-icon> </md-button> </div> </md-toolbar> <div> <md-list> <md-list-item ng-repeat=\"lang in app.config.languages\" ng-click=selectLanguage(lang)> <p>{{ lang.title}}</p> <wb-icon ng-click=saveAs(lang) aria-label=\"Save language as a file\" class=\"md-secondary md-hue-3\">download</wb-icon> <wb-icon ng-click=deleteLanguage(lang) aria-label=\"Delete language\" class=\"md-secondary md-hue-3\">delete</wb-icon> </md-list-item> </md-list> </div> </md-content> </md-sidenav> <md-content flex> <div ng-if=!selectedLanguage layout-padding> Select a language to view/edit translations. </div> <div ng-if=selectedLanguage layout=row> <md-input-container md-no-float class=md-block> <label translate>Key</label> <input ng-model=selectedLanguage.key> </md-input-container> <md-input-container md-no-float class=md-block> <label translate>Title</label> <input ng-model=selectedLanguage.title> </md-input-container> </div> <div layout=column layout-margin> <md-input-container class=md-block flex ng-repeat=\"(key, value) in selectedLanguage.map\"> <label>{{key}}</label> <input ng-model=selectedLanguage.map[key]> </md-input-container> </div> </md-content>"
  );


  $templateCache.put('views/amh-widgets/feature-toolbar-language.html',
    "<md-toolbar wb-border=wbModel.style layout=row layout-align=\"center center\"> <md-button ng-href=home style=margin:0px layout=row> <img ng-if=wbModel.cover height=42px ng-src=\"{{ wbModel.cover }}\" ng-class=\"md-icon-button\"> <md-truncate ng-if=wbModel.label layout-padding> {{ wbModel.label }}</md-truncate> </md-button> <am-wb-feature-button hide show-gt-md ng-repeat=\"feature in wbModel.features | limitTo: 8\" ng-model=feature wb-icon-button=feature.image> <img ng-if=feature.image height=32px ng-src=\"{{ feature.image }}\"> <span ng-if=!feature.image>{{ feature.title }}</span> <md-tooltip ng-if=feature.text>{{ feature.text }}</md-tooltip> </am-wb-feature-button> <span flex></span> <md-menu ng-show=app.config.languages.length ng-controller=AmhLanguagesCtrl> <md-button style=margin:0px aria-label=\"Open language menu\" ng-click=$mdMenu.open($event)> <span translate>Language</span>/{{languageKey}} </md-button> <md-menu-content> <md-menu-item ng-repeat=\"lang in app.config.languages\"> <md-button show-gt-md aria-label=menu ng-href=home/{{lang.key}} ng-click=setLanguage(lang)> <span translate>{{ lang.title }}</span> </md-button> </md-menu-item> </md-menu-content> </md-menu> <md-menu ng-show=wbModel.features.length> <md-button aria-label=\"Open demo menu\" class=md-icon-button ng-click=$mdMenu.open($event)> <wb-icon>more_vert</wb-icon> </md-button> <md-menu-content> <md-menu-item ng-repeat=\"feature in wbModel.features\"> <md-button show-gt-md aria-label=menu ng-click=runAction(feature)> <img height=24px ng-src=\"{{ feature.image }}\"> {{ feature.title }} </md-button> </md-menu-item> </md-menu-content> </md-menu> </md-toolbar>"
  );

}]);
