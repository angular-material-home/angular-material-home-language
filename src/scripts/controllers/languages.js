  /*
   * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
   * 
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   * 
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   * 
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  'use strict';
  angular.module('ngMaterialHomeLanguage')

          /**
           * @ngdoc controller
           * @name AmhLanguagesCtrl
           * @description Mange list of languages
           * 
           * Manages list of languages
           * 
           */
          .controller('AmhLanguagesCtrl', function ($scope, $rootScope, $language, $navigator, FileSaver) {

              function _load() {
                  return $language.languages()
                          .then(function (langs) {
                              ctrl.items = langs.items;
                              return $language.use();
                          })
                          .then(function (keyLang) {
                              $scope.languageKey = keyLang;
                          });
              }

              /**
               * Set current language of app
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - Key of the language
               * @return {promise} to change language
               */
              function setLanguage(lang) {
//		$rootScope.app.setting.language = lang.key;
                  $language.use(lang.key)//$language is defined in mblowfish-core module
                          .then(function () {
                              $navigator.openView('home/' + lang.key);
                          });
              }

              /**
               * Adds new language to app configuration
               * 
               * @memberof AmhLanguagesCtrl
               * @return {promise} to add language
               */
              function addLanguage() {
                  return $language.newLanguage({
                      key: 'Language key',
                      title: 'Language title'
                  });
              }

              /**
               * Remove language form application
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - The Language
               * @return {promise} to delete language
               */
              function deleteLanguage(lang) {
                  return $language.deleteLanguage(lang)
                          .then(function () {
                              if (angular.equals($scope.selectedLanguage, lang)) {
                                  $scope.selectedLanguage = null;
                              }
                          });
              }

              /**
               * Select language
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - The Language
               */
              function selectLanguage(lang) {
                  $scope.selectedLanguage = lang;
                  lang.map = lang.map || {};
                  angular.forEach($rootScope.app.setting.languageMissIds, function (id) {
                      lang.map[id] = lang.map[id] || id;
                  });
              }

              /**
               * Save a language as a file
               * 
               * @memberof AmhLanguagesCtrl
               * @param {object} lang - The Language
               */
              function saveAs(lang) {
                  var MIME_WB = 'application/weburger+json;charset=utf-8';

                  // save  result
                  var dataString = JSON.stringify(lang);
                  var data = new Blob([dataString], {
                      type: MIME_WB
                  });
                  return FileSaver.saveAs(data, 'language.json');
              }

            /**
            * Upload a file as the content of the page
            * 
            * @memberof AmhContentCtrl
            * @return {promiss} to save content value
            */
            function uploadLang(file){
                    var fileInput = document.createElement("input");
                    fileInput.type='file';
                    fileInput.style.display='none';
                    fileInput.onchange=function(event){
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                    var lang = JSON.parse(event.target.result);
                                    $language.newLanguage(lang)
                                        .then(function(lang){
                                                $scope.$apply();
                                         }, function(error){
                                             alert(error);
                                         });
                            };
                            reader.readAsText(event.target.files[0]); 
                    };
                    document.body.appendChild(fileInput);
                    // click Elem (fileInput)
                    var eventMouse = document.createEvent("MouseEvents");
                    eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    fileInput.dispatchEvent(eventMouse);
            }

              
              function close(){
                  $scope.uploadFileIsTrue = false;
              }

              if (!$scope.app) {
                  $scope.app = $rootScope.app;
              }

              // TODO: hadi, This controller should call $language.refresh() after add/remove/update a language

              var ctrl = {};
              $scope.ctrl = ctrl;
              $scope.setLanguage = setLanguage;
              $scope.addLanguage = addLanguage;
              $scope.deleteLanguage = deleteLanguage;
              $scope.selectLanguage = selectLanguage;
              $scope.uploadLang = uploadLang;
              $scope.saveAs = saveAs;
              $scope.close = close;
              _load();
          });